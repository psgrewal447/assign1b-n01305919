﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="assign1b.aspx.cs" Inherits="Assign_1B.assign1b" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Gym Membeship</title>
</head>
<body>
    <form id="form1" runat="server">
        <div>
            <p>Membership Form</p>
            Name:<asp:TextBox runat="server" ID="PersonName" placeholder="Full Name" ></asp:TextBox>
            <asp:RequiredFieldValidator runat="server" ErrorMessage="Enter your name" ControlToValidate="PersonName" ID="validatorName"></asp:RequiredFieldValidator>
            <br />
            E-mail:<asp:TextBox runat="server" ID="PersonalEmail" placeholder="Email"></asp:TextBox>
            <asp:RequiredFieldValidator runat="server" ErrorMessage="Enter your email" ControlToValidate="PersonalEmail" ID="RequiredFieldValidator1"></asp:RequiredFieldValidator>
            <asp:RegularExpressionValidator ID="regexEmailValid" runat="server" ValidationExpression="\w+([-+.]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*" ControlToValidate="PersonalEmail" ErrorMessage="Email format is wrong"></asp:RegularExpressionValidator>
        </div>
        Gender:<div id="Gender" runat="server">
            <asp:RadioButton runat="server" Text="Male" GroupName="Gender"/>
            <asp:RadioButton runat="server" Text="Female" GroupName="Gender"/>
        </div>
            Extra Activity:<div id="OtherActivity" runat="server">
                 <asp:CheckBox runat="server" ID="activity1" Text="Aqua Aerobics" />
                 <asp:CheckBox runat="server" ID="activity2" Text="Boxing" />
                 <asp:CheckBox runat="server" ID="activity3" Text="Dance" />
        </div>
        <div>
                Membership Type:<asp:DropDownList runat="server" ID="MembershipType">
                <asp:ListItem Value="S" Text="Silver"></asp:ListItem>
                <asp:ListItem Value="G" Text="Gold"></asp:ListItem>
                <asp:ListItem Value="P" Text="Platnium"></asp:ListItem>
            </asp:DropDownList>
        
        <asp:Button OnClick="submit" Text="submit" runat="server" />

        </div>
            
    </form>
</body>
</html>
