﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="gymmembership.aspx.cs" Inherits="assignwad1.gymmembership" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
        <asp:ValidationSummary id="vsumry"  DisplayMode="BulletList" EnableClientScript="true" HeaderText="You must enter a value in the following fields:" runat="server"/>
        <div>
            <p><B>Gym Membership Form </B></p>
            Name:<asp:TextBox runat="server" ID="PersonName" placeholder="Full Name" ></asp:TextBox>
            <asp:RequiredFieldValidator runat="server" ErrorMessage="Enter your name" ControlToValidate="PersonName" ID="Actualname"></asp:RequiredFieldValidator>
            <br />
            E-mail:<asp:TextBox runat="server" ID="PersonalEmail" placeholder="Email"></asp:TextBox>
            <asp:RequiredFieldValidator runat="server" ErrorMessage="Enter your email" ControlToValidate="PersonalEmail" ID="realemail"></asp:RequiredFieldValidator>
            <asp:RegularExpressionValidator ID="regexEmailValid" runat="server" ValidationExpression="\w+([-+.]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*" ControlToValidate="PersonalEmail" ErrorMessage="Email format is wrong"></asp:RegularExpressionValidator>
           <br />
            Mobile:<asp:TextBox runat="server" ID="Mobileno" placeholder="Moblie no."></asp:TextBox>
           <br />
            Age:<asp:TextBox runat="server" ID="memberage" placeholder="Enter your name" ></asp:TextBox>
            <asp:RangeValidator runat="server" id="age1" controltovalidate="memberage" minimumvalue="18" maximumvalue="50" errormessage="Your are not eligible for membership only(18-50)" />
        </div>
        Gender:<div id="Gender" runat="server">
            <asp:RadioButton runat="server" Text="Male" GroupName="Gender"/>
            <asp:RadioButton runat="server" Text="Female" GroupName="Gender"/>
        </div>
           <br />

            Extra Activity:<div id="OtherActivity" runat="server">
                 <asp:CheckBox runat="server" ID="activity1" Text="Aqua Aerobics" />
                 <asp:CheckBox runat="server" ID="activity2" Text="Boxing" />
                 <asp:CheckBox runat="server" ID="activity3" Text="Dance" />
                 <asp:CheckBox runat="server" ID="activity4" Text="Jump Rope " />
                 <asp:CheckBox runat="server" ID="activity5" Text="Dribble Knockout " />
            <br />
         </div>
        <br />
        <div>
                Membership Type:<asp:DropDownList runat="server" ID="MembershipType">
                <asp:ListItem Value="S" Text="Silver"></asp:ListItem>
                <asp:ListItem Value="G" Text="Gold"></asp:ListItem>
                <asp:ListItem Value="P" Text="Platnium"></asp:ListItem>
               
            </asp:DropDownList>
        </div>
            <br />
            <asp:Button runat="server" ID="myButton"  Text="Submit" OnClick="membershipid"></asp:Button >
            
         
            <div runat="server" id="result"></div>
            </form>
    </body>
</html>
