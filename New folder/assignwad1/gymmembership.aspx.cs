﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace assignwad1
{
    public partial class gymmembership : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }
        protected void membershipid(object sender, EventArgs e)
        {
             if (!Page.IsValid)
            {

                return;
            }
      
            string clientPersonName = PersonName.Text.ToString();
            string clientPersonalEmail = PersonalEmail.Text.ToString();
            string clientMobileno = Mobileno.Text.ToString();
            string clientmemberage = memberage.Text.ToString();
            client addclient = new client();
            addclient.ClientName = clientPersonName;
            addclient.Clientmobile = clientMobileno;
            addclient.ClientEmail = clientPersonalEmail;
            addclient.ClientAge = clientmemberage;

     string membershiptyp = MembershipType.SelectedItem.Text.ToString();

      List<string> extraactivt = new List<string>();
         foreach(Control control in OtherActivity.Controls)
            {
            if(control.GetType() ==typeof(CheckBox))
                {
                  CheckBox activity = (CheckBox)control;
                    if(activity.Checked)
                    {
                        extraactivt.Add(activity.Text);
                    }
                }

            }
            extras mt = new extras(membershiptyp, extraactivt);
            joiningpass jbi = new joiningpass (addclient,mt);
            result.InnerHtml = jbi.Reciptpass();
        }

    }
}